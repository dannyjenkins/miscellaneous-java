public class Fibonacci {

	public static void sleep(int sleepTime) {
		try {
			Thread.sleep(sleepTime);
		} catch (InterruptedException ex) {
			Thread.currentThread().interrupt();
		}
	}

	public static void main(String[] args) {
		int sleepTime = 200;
		int x = 1;
		int y = 0;
		while (true) {
			x = x + y;
			y = x + y;

			System.out.println(x);
			sleep(sleepTime);

			System.out.println(y);
			sleep(sleepTime);
		}
	}
}
