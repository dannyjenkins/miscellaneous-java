public class FizzBuzz {

	public static void main(String[] args) {
		for (int check = 1; check <= 1000000; check++) {
			if (check % 5 == 0 && check % 3 == 0) {
				System.out.println("FizzBuzz");
			} else if (check % 5 == 0) {
				System.out.println("Buzz");
			} else if (check % 3 == 0) {
				System.out.println("Fizz");
			} else {
				System.out.println(check);
			}
		}
	}
}
