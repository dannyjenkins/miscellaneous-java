import java.util.Random;
import java.util.Scanner;

public class PasswordGen {

	public static char charSel(String s) {
		Random rand  = new Random();
		int    index = rand.nextInt(s.length());
		return s.charAt(index);
	}

	public static void main(String[] args) {
		String  charSet = "";
		String  pass    = "";
		Scanner reader  = new Scanner(System.in);

		System.out.println("Enter string length: ");
		int stringLength = reader.nextInt();
		System.out.println("Do you want capital letters? (yes=1, no=0)");
		int capitals     = reader.nextInt();
		System.out.println("Do you want lowercase letters? (yes=1, no=0)");
		int lowercase    = reader.nextInt();
		System.out.println("Do you want numbers? (yes=1, no=0)");
		int number       = reader.nextInt();
		reader.close();

		if (capitals  == 1) charSet += "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		if (lowercase == 1) charSet += "abcdefghijklmnopqrstuvwxyz";
		if (number    == 1) charSet += "0123456789";

		for (int i = 0; i < stringLength; i++) {
			pass += charSel(charSet);
		}

		System.out.println(pass);
	}
}
