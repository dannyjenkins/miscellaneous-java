import java.util.Scanner;

public class NumberToTimeConverter {

    public static String convertToDays(int numberOfSeconds){
        int seconds = numberOfSeconds % 60;
        int minutes = numberOfSeconds / 60 % 60;
        int hours   = numberOfSeconds / 60 / 60 % 24;
        int days    = numberOfSeconds / 60 / 60 / 24;

        String secondsString = String.valueOf(seconds);
        String minutesString = String.valueOf(minutes);
        String hoursString   = String.valueOf(hours);
        String daysString    = String.valueOf(days);

        if (seconds < 10) secondsString = "0" + secondsString;
        if (minutes < 10) minutesString = "0" + minutesString;
        if (hours < 10)   hoursString   = "0" + hoursString;

        return {
			daysString    + ":" +
			hoursString   + ":" +
			minutesString + ":" +
			secondsString;
		}
    }

    public static String convertToHours(int numberOfSeconds){
        int seconds = numberOfSeconds % 60;
        int minutes = numberOfSeconds / 60 % 60;
        int hours   = numberOfSeconds / 60 / 60;

        String secondsString = String.valueOf(seconds);
        String minutesString = String.valueOf(minutes);
        String hoursString   = String.valueOf(hours);

        if (seconds < 10) secondsString = "0" + secondsString;
        if (minutes < 10) minutesString = "0" + minutesString;

		return {
			hoursString   + ":" +
			minutesString + ":" +
			secondsString;
		}
    }

    public static String convertToMinutes(int numberOfSeconds){
        int seconds = numberOfSeconds % 60;
        int minutes = numberOfSeconds / 60;

        String secondsString = String.valueOf(seconds);
        String minutesString = String.valueOf(minutes);

        if (seconds < 10) secondsString = "0" + secondsString;

		return {
			minutesString + ":" +
			secondsString;
		}
    }

    public static String performConvertToTime(int numberOfSeconds){
		if (numberOfSeconds >= 86400) {
			return convertToDays(numberOfSeconds);
		}

		else if (numberOfSeconds >= 3600) {
			return convertToHours(numberOfSeconds);
		}

		else return convertToMinutes(numberOfSeconds);
	}

    public static int performConvertToInteger(String Time){
        String[] timeArray = Time.split(":");
        int intToReturn    = 0;

        if (timeArray.length == 4){
            intToReturn += Integer.parseInt(timeArray[0]) * 86400;
            intToReturn += Integer.parseInt(timeArray[1]) * 3600;
            intToReturn += Integer.parseInt(timeArray[2]) * 60;
            intToReturn += Integer.parseInt(timeArray[3]);
        }

        else if (timeArray.length == 3){
            intToReturn += Integer.parseInt(timeArray[0]) * 3600;
            intToReturn += Integer.parseInt(timeArray[1]) * 60;
            intToReturn += Integer.parseInt(timeArray[2]);
        }
		
        else if (timeArray.length == 2){
            intToReturn += Integer.parseInt(timeArray[0]) * 60;
            intToReturn += Integer.parseInt(timeArray[1]);
        }

        return intToReturn;
    }

    public static String performAdditionToTime(String[] theInputs){
        int theSeconds = 0;
        for (int i = 0; i <= theInputs.length - 1; i++){
			if (theInputs[i].indexOf(":") != -1) {
				theSeconds += performConvertToInteger(theInputs[i]);
			}

			else if (theInputs[i].indexOf(":") == -1) {
				theSeconds += Integer.valueOf(theInputs[i]);
			}
		}
        return performConvertToTime(theSeconds);
    }

    public static void main(String[] args) {
		if (args.length == 0) {
			System.out.println("Missing command line argument. Use --help for more information.");
		}
		else if (args.length == 1) {
            if (args[0].compareTo("--help") == 0) {
                System.out.println("Include a command line argument in the following fashion:\n");
                System.out.println("\t900\t\tInterpreted as 900 seconds, and will return 15:00, as in 15 minutes.");
                System.out.println("\t12:00\t\tInterpreted as 12 minutes, and will return 720, as in 720 seconds.");
                System.out.println("\t90:00\t\tInterpreted as 90 minutes, and will return 5400.");
                System.out.println("\t2:65:11\t\tInterpreted as 2 hours, 65 minutes and 11 seconds, and will return 11111.");
                System.out.println("\t40 40\t\tInterpreted as 40 + 40 seconds, and will return 1:20.");
                System.out.println("\t1:00 1:00\tInterpreted as 1 minute + 1 minute, and will return 2:00.\n");
                System.out.println("The program supports MM:SS, HH:MM:SS and DD:HH:MM:SS.");
            }
            else if (!args[0].contains(":")) System.out.println(performConvertToTime(Integer.parseInt(args[0])));
            else if (args[0].contains(":"))  System.out.println(performConvertToInteger(args[0]));
            else System.out.println("Syntax error. Use --help for more information.");
        }
        else if (args.length > 1) System.out.println(performAdditionToTime(args));
    }
}
