import java.util.ArrayList;

public class Prime {
	public static boolean isPrime(long x) {
		if      (x == 1) return false;
		else if (x == 2) return true;
		else {
			// For-loop that performs divisions and if it finds a
			// divisor that leads to a whole division, it causes the
			// method to return false
			for (long check = 2; check <= x/2; check++){
				if (x % check == 0) return false;
			}
			// if the for-loop completes without having found a
			// divisor that leads to a whole division, the method
			// returns true
			return true;
		}
	}

	// Method that is identical to the one above except it uses an
	// ArrayList to store previously found primes for later
	// comparisons
	public static boolean isPrime(long x, ArrayList<Long> theList) {
		if      (x == 1) return false;
		else if (x == 2) theList.add(x);
		else {
			for (long check : theList) {
				if (x % check == 0) return false;
			}
		}
		theList.add(x);
		System.out.println(x);
		return true;
	}
}
