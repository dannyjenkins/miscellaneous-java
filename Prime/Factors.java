import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Factors {
	public static void main(String[] args) {
		Prime   prime  = new Prime();
		Scanner reader = new Scanner(System.in);

		System.out.print("Enter a number to find its factors: ");
		long checknumber = reader.nextLong();
		List<Long> Factors = new ArrayList<>();

		if (!prime.isPrime(checknumber)) {
			long check = 2;
			while (check <= checknumber) {
				if (checknumber % check == 0) {
					Factors.add(check);
					checknumber = checknumber / check;
					check = 2;
				} else check++;
			}
			System.out.println(Factors);
		} else System.out.println(checknumber + " is a prime number.");
	}
}
