import java.util.HashMap;
import java.util.Map;

public class PrimeEnding {
	public static void main(String[] args){

		Prime prime = new Prime();

		if (args.length == 0){
			System.out.println("Error: positive integer argument required");
			System.exit(1);
		}
		Map<String, Integer> numValues = new HashMap<String, Integer>();
		for (int i = 1; i < 10; i++){
			String iStr = String.valueOf(i);
			numValues.put(iStr, 0);
		}
		int upperLimit = Integer.valueOf(args[0]);
		for (int thisNum = 2; thisNum <= upperLimit; thisNum++){
			if (prime.isPrime(thisNum)){
				String thisNumStr = String.valueOf(thisNum);
				String lastDigit = thisNumStr.substring(thisNumStr.length() - 1);
				Integer count = numValues.get(lastDigit);
				numValues.put(lastDigit, count + 1);
			}
			System.out.println(numValues);
		}
	}
}
