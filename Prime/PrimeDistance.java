public class PrimeDistance {

	/**
	 * Method that finds the distance to the previous prime for any given number.
	 * @param x A number
	 * @return The previous prime, i.e. the largest prime which is smaller than the given number
	 */
	public static int distancePrevPrime(int x){

		Prime prime = new Prime();

		for (int i = x-1; i >= 2; i--){
			if (prime.isPrime(i)) return (x-i);
		}

		return (1);
	}

	public static void main(String[] args){

		Prime prime = new Prime();

		// the check range will be between the lower limit args[0] and
		// the upper limit args[1]
		int ll = Integer.parseInt(args[0]);
		int ul = Integer.parseInt(args[1]);;
		for (int primecheck = ll; primecheck <= ul; primecheck++){
			if (prime.isPrime(primecheck)){
				System.out.println(primecheck + " " + distancePrevPrime(primecheck));
			}
		}
	}
}
