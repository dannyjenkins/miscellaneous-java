import java.util.ArrayList;

public class PrimeQuick {
	public static void main(String[] args) {
		Prime prime      = new Prime();
		int   upperLimit = Integer.parseInt(args[0]);
		ArrayList<Long> listOfPrimes = new ArrayList<Long>();

		for (int i = 1; i < upperLimit; i++){
			prime.isPrime(i, listOfPrimes);
		}
	}
}
